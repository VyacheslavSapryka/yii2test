<?php
use yii\helpers\Html;
use yii\widgets;
?>



<?php
echo \yii\grid\GridView::widget(
    [
        /**
         * Экземпляр класса, который реализует \yii\data\DataProviderInterface. В нашем случае ActiveDataProvider
         */
        'dataProvider' => $dataProvider,
        /**
         * Модель которая используется для фильтрации. Она нужна для отображения input-ов поиска в шапке таблицы
         */
        'filterModel' => new \app\models\Page(),
        /**
         * Список колонок которые необходимо отобразить
         */
        'columns' => [
            /**
             * Столбец нумерации. Отображает порядковый номер строки
             */
            [
                'class' => \yii\grid\SerialColumn::class,
            ],
            /**
             * Перечисленные ниже поля модели отображаются как колонки с данными без изменения
             */
            'id',
            'slug',
            'url',
            'name',
            /**
             * Произвольная колонка с определенной логикой отображения и фильтром в виде выпадающего списка
             */
            [
                /**
                 * Название поля модели
                 */
                'attribute' => 'active',
                /**
                 * Формат вывода.
                 * В этом случае мы отображает данные, как передали.
                 * По умолчанию все данные прогоняются через Html::encode()
                 */
                'format' => 'raw',
                /**
                 * Переопределяем отображение фильтра.
                 * Задаем выпадающий список с заданными значениями вместо поля для ввода
                 */
                'filter' => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                /**
                 * Переопределяем отображение самих данных.
                 * Вместо 1 или 0 выводим Yes или No соответственно.
                 * Попутно оборачиваем результат в span с нужным классом
                 */
                'value' => function ($model, $key, $index, $column) {
                    $active = $model->{$column->attribute} === 1;
                    return \yii\helpers\Html::tag(
                        'span',
                        $active ? 'Yes' : 'No',
                        [
                            'class' => 'label label-' . ($active ? 'success' : 'danger'),
                        ]
                    );
                },
            ],
            /**
             * Пример краткого описания настроек столбца.
             * Данный способ описания имеет следующий вид attribute_name:output_format:attribute_label.
             */
            'created_at:datetime:Crete datetime',
            /**
             * Пример использования форматера
             */
            [
                /**
                 * Имя аттрибута модели
                 */
                'attribute' => 'updated_at',
                /**
                 * Формат вывода
                 */
                'format' => ['datetime', 'php:Y-m-d h:i:s'],
            ],
            /**
             * Колонка кнопок действий
             */
            [
                /**
                 * Указываем класс колонки
                 */
                'class' => \yii\grid\ActionColumn::class,
                /**
                 * Определяем набор кнопочек. По умолчанию {view} {update} {delete}
                 */
                'template' => '{update} {delete}',
            ],
        ],
    ]
);
?>