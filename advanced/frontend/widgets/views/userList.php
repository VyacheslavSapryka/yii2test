<?php

    //print_r($userlist);


    $page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
    $total = count( $userlist );                                            //total users in array
    $limit = 2;                                                             //show users per page
    $totalPages = ceil( $total/ $limit );                                   //total pages
    $page = max($page, 1);                                                  //get 1 page when $_GET['page'] <= 0
    $page = min($page, $totalPages);                                        //get last page when $_GET['page'] > $totalPages
    $offset = ($page - 1) * $limit;
    if( $offset < 0 ) $offset = 0;

    $userListDataArray = array_slice( $userlist, $offset, $limit );
    //print_r($yourDataArray);

    foreach ($userListDataArray as $key=>$value)
    {
        echo "<p>";
        echo "id: $value[id]";
        echo "<br>";
        echo "$value[username]";
        echo "<br>";
        echo "$value[email]";
        echo "<br>";
        echo Yii::$app->formatter->asDate("$value[created_at]");
        echo "<br>";
        echo Yii::$app->formatter->asDate("$value[updated_at]");
        echo "<p/>";
    }

    $link = '?page=%d';
    $pagerContainer = '<div>';
    if( $totalPages != 0 )
    {
        if( $page == 1 )
        {
            $pagerContainer .= '';
        }
        else
        {
            $pagerContainer .= sprintf( '<a href="' . $link . '" style="color: #c00"> &#171; prev page</a>', $page - 1 );
        }
        $pagerContainer .= ' <span> page <strong>' . $page . '</strong> from ' . $totalPages . '</span>';
        if( $page == $totalPages )
        {
            $pagerContainer .= '';
        }
        else
        {
            $pagerContainer .= sprintf( '<a href="' . $link . '" style="color: #c00"> next page &#187; </a>', $page + 1 );
        }
    }
    $pagerContainer .= '</div>';
    echo "<br>";
    echo "<br>";
    echo $pagerContainer;
?>
