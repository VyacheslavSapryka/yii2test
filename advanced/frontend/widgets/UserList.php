<?php

namespace frontend\widgets;
use yii\base\Widget;
use common\models\User;


class UserList extends Widget
{
    function run()
    {
        $userlist = User:: find()->asArray()->all();
        return $this->render('userList', ['userlist'=>$userlist]);
    }
}
?>